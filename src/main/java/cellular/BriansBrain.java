package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;


    public BriansBrain(int rows, int cols) {
        currentGeneration = new CellGrid(rows, cols, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row,col);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                int randInt = random.nextInt(3);
                if (randInt == 0) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else if(randInt == 1) {
                    currentGeneration.set(row, col, CellState.DEAD);
                } else {
                    currentGeneration.set(row,col,CellState.DYING);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for(int i = 0; i < numberOfRows(); i++) {
            for(int j = 0; j < numberOfColumns(); j++) {
                nextGeneration.set(i,j,getNextCell(i,j));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int neighborCount = countNeighbors(row,col,getCellState(row,col));
        if (getCellState(row,col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        } else if(getCellState(row,col).equals(CellState.DEAD)) {
            return neighborCount == 2 ? CellState.ALIVE : CellState.DEAD;
        } else {
            return CellState.DEAD;
        }
    }

    private int countNeighbors(int row, int col, CellState state) {
        int countedNeighbors = 0;
        for(int i = row-1; i <= row+1; i++) {
            for(int j = col-1; j <= col+1; j++) {
                try {
                    CellState cellState = getCellState(i,j);
                    if(cellState.equals(CellState.ALIVE)) {
                        countedNeighbors++;
                    }
                } catch (IndexOutOfBoundsException e) {}
            }
        }
        if(state.equals(CellState.ALIVE)) {
            countedNeighbors--;
        }
        return countedNeighbors;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
