package datastructure;

import cellular.CellState;

import java.sql.Array;
import java.util.ArrayList;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    ArrayList<CellState> grid;
    CellState initialState;
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        grid = new ArrayList<>();

        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid.add(initialState);
            }
        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row >= rows || column >= cols || row < 0 || column < 0) {
            throw new IndexOutOfBoundsException();
        }
        grid.set(indexOf(row,column),element);
    }

    @Override
    public CellState get(int row, int column) {
        if(row >= rows || column >= cols || row < 0 || column < 0) {
            throw new IndexOutOfBoundsException();
        }
        return grid.get(indexOf(row,column));
    }

    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(this.rows,this.cols,this.initialState);
        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                newGrid.set(i,j,grid.get(indexOf(i,j)));
            }
        }
        return newGrid;
    }

    private Integer indexOf(int row, int col) {
        return col + row*numColumns();
    }

}
